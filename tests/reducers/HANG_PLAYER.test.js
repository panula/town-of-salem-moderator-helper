import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import {roles, roleKeys, effects } from '../../src/constants/roles';
import create from '../support/create.js';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('hangs player', () => {
  initialState.players = [ create.player(1) ];

  const newState = reducers(initialState, {type: actionTypes.HANG_PLAYER, player: 1});

  expect(newState.players[0].alive).toBe(false);
  expect(newState.log[0]).toBe("Day 1: test was hanged.")
});

it('hangs jester - jester wins', () => {
  initialState.players = [ create.player(1, roleKeys.JESTER) ];

  const newState = reducers(initialState, {type: actionTypes.HANG_PLAYER, player: 1});

  expect(newState.players[0].alive).toBe(false);
  expect(newState.log[0]).toBe("Day 1: test was hanged. test won.")
});

it('hangs executioner target - executioner wins', () => {
  initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.EXECUTIONER) ];
  initialState.permanentEffects = [{ to: 1, from: 2, effect: effects.EXECUTE, power: 0}];

  const newState = reducers(initialState, {type: actionTypes.HANG_PLAYER, player: 1});

  expect(newState.players[0].alive).toBe(false);
  expect(newState.log[0]).toBe("Day 1: test was hanged. test won.")
});


it('hangs executioner target - dead executioner does not win', () => {
  initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.EXECUTIONER) ];
  initialState.players[1].alive = false;
  initialState.permanentEffects = [{ to: 1, from: 2, effect: effects.EXECUTE, power: 0}];

  const newState = reducers(initialState, {type: actionTypes.HANG_PLAYER, player: 1});

  expect(newState.players[0].alive).toBe(false);
  expect(newState.log[0]).toBe("Day 1: test was hanged.")
});


