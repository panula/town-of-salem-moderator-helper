import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import roles from '../../src/constants/roles';
import create from '../support/create';
import {roleKeys} from '../../src/constants/roles';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('changes name', () => {
  initialState.players = [create.player(1, roleKeys.DOCTOR, 'guy')];

  const newState = reducers(initialState, {type: actionTypes.CHANGE_NAME, name: 'elon musk', player: 1});

  expect(newState.players.length).toBe(1);
  expect(newState.players[0]).toEqual({ id: 1, name: 'elon musk', alive: true, role: roles.DOCTOR });
});

