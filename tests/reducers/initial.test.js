import reducers from '../../src/store/Reducers';

const initialState = reducers(undefined, {});

it('initializes', () => {
  expect(initialState).toEqual({
    players: [],
    night: 1,
    started: false,
    effects: [],
    finalEffects: {},
    log: [],
    permanentEffects: []
  });
});
