import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import create from '../support/create';
import {roleKeys, roles} from '../../src/constants/roles';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('new game resets game state but keeps players', () => {
  initialState.players = [create.player(1, roleKeys.DOCTOR), create.player(2, roleKeys.MAFIOSO)];
  initialState.night = 5;
  initialState.effects = [{}, {}];
  initialState.log = ['', ''];
  initialState.started = true;

  const newState = reducers(initialState, {type: actionTypes.NEW_GAME});

  expect(newState.players.length).toBe(2);
  expect(newState.players[0]).toEqual({ id: 1, name: 'test', alive: true, role: roles.TOWNIE });
  expect(newState.players[1]).toEqual({ id: 2, name: 'test', alive: true, role: roles.TOWNIE });
  expect(newState.effects.length).toBe(0);
  expect(newState.log.length).toBe(0);
  expect(newState.night).toBe(1);
  expect(newState.started).toBe(false);
});

it('new game resurrects players', () => {
  initialState.players = [create.player(1, roleKeys.DOCTOR)];
  initialState.started = true;
  initialState.players[0].alive = false;

  const newState = reducers(initialState, {type: actionTypes.NEW_GAME});

  expect(newState.players.length).toBe(1);
  expect(newState.players[0]).toEqual({ id: 1, name: 'test', alive: true, role: roles.TOWNIE });
});



