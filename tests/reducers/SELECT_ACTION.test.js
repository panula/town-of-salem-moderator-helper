import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import {roles, effects, factions, roleKeys } from '../../src/constants/roles';
import create from '../support/create.js';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('can select action', () => {
  initialState.players = [ create.player(1, roleKeys.DOCTOR), create.player(roleKeys.TOWNIE)];
  
  const newState = reducers(initialState, {type: actionTypes.SELECT_ACTION, to: 2, from: 1, effect: effects.HEAL, power: 50});

  expect(newState.effects.length).toBe(1);
  expect(newState.effects[0]).toEqual({ to: 2, from: 1, effect: effects.HEAL, power: 50});
});

it('mafia shares action', () => {
  initialState.players = [create.player(1, roleKeys.MAFIOSO), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.TOWNIE)];
  
  const newState = reducers(initialState, {type: actionTypes.SELECT_ACTION, to: 3, from: 1, effect: effects.MURDER, power: 50});

  expect(newState.effects.length).toBe(2);
  expect(newState.effects[0]).toEqual({ to: 3, from: 1, effect: effects.MURDER, power: 50});
  expect(newState.effects[1]).toEqual({ to: 3, from: 2, effect: effects.MURDER, power: 50});
});

it('witch can curse', () => {
  initialState.players = [create.player(1, roleKeys.MAFIOSO), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.WITCH)];
  
  const newState = reducers(initialState, {type: actionTypes.SELECT_ACTION, to: 1, from: 3, effect: effects.CURSE, power: 50});

  expect(newState.effects.length).toBe(1);
  expect(newState.effects[0]).toEqual({ to: 1, from: 3, effect: effects.CURSE, power: 50});
});


it('executioner can choose target', () => {
  initialState.players = [create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.EXECUTIONER)];
  
  const newState = reducers(initialState, {type: actionTypes.SELECT_ACTION, to: 1, from: 3, effect: effects.EXECUTE, power: 0});

  expect(newState.effects.length).toBe(0);
  expect(newState.permanentEffects.length).toBe(1);
  expect(newState.permanentEffects[0]).toEqual({ to: 1, from: 3, effect: effects.EXECUTE, power: 0});
});


it('executioner can choose only one target', () => {
  initialState.players = [create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.EXECUTIONER)];
  initialState.permanentEffects = [{ to: 1, from: 3, effect: effects.EXECUTE, power: 0}];

  const newState = reducers(initialState, {type: actionTypes.SELECT_ACTION, to: 2, from: 3, effect: effects.EXECUTE, power: 0});

  expect(newState.effects.length).toBe(0);
  expect(newState.permanentEffects.length).toBe(1);
  expect(newState.permanentEffects[0]).toEqual({ to: 2, from: 3, effect: effects.EXECUTE, power: 0});
});


