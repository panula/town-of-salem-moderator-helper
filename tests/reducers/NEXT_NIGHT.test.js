import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import {effects, roles, roleKeys} from '../../src/constants/roles';
import create from '../support/create.js';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('next night - nothing', () => {
  initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO) ];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
});


it('next night - mafia murders', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.DOCTOR) ];
 initialState.effects = [{ to: 1, from: 2, effect: effects.MURDER }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Player test was murdered.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toBe(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - heal prevents murder', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.DOCTOR) ];
 initialState.effects = [{ to: 1, from: 2, effect: effects.MURDER }, { to: 1, from: 3, effect: effects.HEAL }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(3);
  expect(newState.effects.length).toBe(0);
  expect(newState.permanentEffects.length).toBe(0);
});


it('next night - curse', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.TOWNIE), create.player(3, roleKeys.WITCH) ];
 initialState.effects = [{ to: 1, from: 3, effect: effects.CURSE }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(3);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects[0]).toEqual({ to: 1, from: 3, effect: effects.CURSE});
});

it('next night - curse remains', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.TOWNIE), create.player(3, roleKeys.WITCH), create.player(4, roleKeys.MAFIOSO) ];
 initialState.effects = [{ to: 1, from: 3, effect: effects.CURSE }];
 initialState.permanentEffects = [{ to: 2, from: 3, effect: effects.CURSE }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(4);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(2);
});

it('next night - usages left decreases', () => {
 initialState.players = [ create.player(1, roleKeys.DOCTOR), create.player(2, roleKeys.MAFIOSO) ];
 initialState.players[0].usagesLeft = 2;
 initialState.effects = [{ to: 1, from: 1, effect: effects.HEAL }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toEqual(0);
  expect(newState.players[0].usagesLeft).toBe(1);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - mafia wins', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.TOWNIE), create.player(3, roleKeys.MAFIOSO), create.player(4, roleKeys.MAFIOSO) ];
 initialState.players[0].alive = false;
 initialState.effects = [{ to: 2, from: 3, effect: effects.MURDER }];
 initialState.night = 3;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 3: Player test was murdered. Mafia wins.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - town wins', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.TOWNIE), create.player(3, roleKeys.MAFIOSO) ];
 initialState.players[2].alive = false;
 initialState.night = 3;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 3: Town wins.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - serial killer wins', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.TOWNIE), create.player(3, roleKeys.SERIAL_KILLER) ];
 initialState.players[0].alive = false;
 initialState.effects = [{ to: 2, from: 3, effect: effects.MURDER }];
 initialState.night = 3;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 3: Player test was murdered. Serial killer wins.')
  expect(newState.players.filter(p => p.alive).length).toBe(1);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - witch wins', () => {
 initialState.players = [ create.player(1, roleKeys.TOWNIE), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.WITCH) ];
 initialState.permanentEffects = [{ to: 1, from: 3, effect: effects.CURSE }];
 initialState.effects = [{ to: 2, from: 3, effect: effects.CURSE }];
 initialState.night = 3;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 3: Witch wins.')
  expect(newState.players.filter(p => p.alive).length).toBe(3);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(2);
});

it('next night - veteran on alert kills first mafioso', () => {
 initialState.players = [ create.player(1, roleKeys.VETERAN), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.MAFIOSO) ];
 initialState.effects = [
   { to: 1, from: 1, effect: effects.ALERT },
   { to: 1, from: 2, effect: effects.MURDER },
   { to: 1, from: 3, effect: effects.MURDER }
   ];
 
  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Player test was murdered.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.players[0].alive).toBe(true);
  expect(newState.players[1].alive).toBe(false);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - mafia kills veteran not on alert', () => {
  initialState.players = [ create.player(1, roleKeys.VETERAN), create.player(2, roleKeys.MAFIOSO), create.player(3, roleKeys.TOWNIE) ];
  initialState.effects = [{ to: 1, from: 1, effect: effects.ALERT }];
  initialState.effects = [{ to: 1, from: 2, effect: effects.MURDER }];;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Player test was murdered.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.players[0].alive).toBe(false);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - survivor cannot be killed', () => {
  initialState.players = [ create.player(1, roleKeys.SURVIVOR), create.player(2, roleKeys.SERIAL_KILLER) ];
  initialState.effects = [{ to: 1, from: 2, effect: effects.MURDER }];;

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Nothing happened.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(0);
});

it('next night - executioner target killed - executioner turns jester', () => { 
  initialState.players = [ create.player(1, roleKeys.TOWNIE, 'test 1'), create.player(2, roleKeys.EXECUTIONER, 'test 2'), create.player(3, roleKeys.MAFIOSO) ];
  initialState.permanentEffects = [{ to: 1, from: 2, effect: effects.EXECUTE }];
  initialState.effects = [{ to: 1, from: 3, effect: effects.MURDER }];

  const newState = reducers(initialState, {type: actionTypes.NEXT_NIGHT});

  expect(newState.log.length).toBe(1);
  expect(newState.log[0]).toBe('Night 1: Player test 1 was murdered. test 2 turned jester.')
  expect(newState.players.filter(p => p.alive).length).toBe(2);
  expect(newState.effects.length).toEqual(0);
  expect(newState.permanentEffects.length).toBe(1);
});




