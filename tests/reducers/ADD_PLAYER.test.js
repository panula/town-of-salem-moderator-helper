import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import roles from '../../src/constants/roles';

let initialState = null;
beforeEach(() => initialState = Object.assign({}, reducers(undefined, {})));

it('adds player', () => {
  const newState = reducers(initialState, {type: actionTypes.ADD_PLAYER, name: 'elon musk'});

  expect(newState.players.length).toBe(1);
  expect(newState.players[0]).toEqual({
    id: 1,
    name: 'elon musk 1',
    alive: true,
    role: roles.TOWNIE
  });
});

it('adds second player', () => {
  const state1 = reducers(initialState, {type: actionTypes.ADD_PLAYER, name: 'elon musk'});
  const newState = reducers(state1, {type: actionTypes.ADD_PLAYER, name: 'elon musk'});

  expect(newState.players.length).toBe(2);
  expect(newState.players[1]).toEqual({
    id: 2,
    name: 'elon musk 2',
    alive: true,
    role: roles.TOWNIE
  });
});

it('cannot add 16th player', () => {
  initialState.players = new Array(15);
  const newState = reducers(initialState, {type: actionTypes.ADD_PLAYER, name: 'elon musk'});

  expect(newState.players.length).toBe(15);
});


