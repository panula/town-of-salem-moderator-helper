import reducers from '../../src/store/Reducers';
import actionTypes from '../../src/store/actionTypes';
import roles from '../../src/constants/roles';
import create from '../support/create.js';

const initialState = reducers(undefined, {});

it('toggle alive - player is alive', () => {
  initialState.players = [ create.player(1) ];

  const newState = reducers(initialState, {type: actionTypes.TOGGLE_ALIVE, player: 1});

  expect(newState.players[0].alive).toBe(false);
});

it('toggle alive - player is not alive', () => {
  initialState.players = [ create.player(1) ];
  initialState.players[0].alive = false;

  const newState = reducers(initialState, {type: actionTypes.TOGGLE_ALIVE, player: 1});

  expect(newState.players[0].alive).toBe(true);
});


