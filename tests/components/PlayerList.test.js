import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'

import { PlayerList2, PlayerList,  mapStateToProps } from '../../src/components/PlayerList/PlayerList'


it('calculates props - final effect - murder vs heal', () => {

  const props = mapStateToProps({app: { present: {
    players: [{ id: 1, role: {value: 0}}],
    night: 1,
    started: true,
    effects: [
      {to:1, power: 50, effect: 'MURDER'}, 
      {to:1, power: 100, effect: 'HEAL'}],
    log: [],
    permanentEffects: []
  }}});

  expect(props.players[0].effects.length).toBe(2);
  expect(props.players[0].finalEffect.effect).toBe('HEAL');

});

it('calculates props - deck value', () => {
  const props = mapStateToProps({app: { present: {
    players: [
      { id: 1, role: {value: 1}},
      { id: 2, role: {value: 4}},
      { id: 3, role: {value: -4}},
      ],
    night: 1,
    started: true,
    effects: [],
    log: [],
    permanentEffects: []
  }}});

  expect(props.deckValue).toBe(1);
});


it('renders without crashing', () => {

  const div = document.createElement('div');
  ReactDOM.render(PlayerList({
    players: [],
    night: 1,
    started: false,
    effects: [],
    finalEffects: {},
    log: []
  }), div);
});



