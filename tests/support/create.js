import {roles} from '../../src/constants/roles'

const create = {
    player: (id, role = 'TOWNIE', name = 'test') => {
        return {name: name, role: roles[role], alive: true, id: id};
    }
};

export default create;