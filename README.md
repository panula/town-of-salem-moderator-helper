# README #

This project is meant for inventory/game tracking helper for board game Town of Salem's moderator role. See https://www.kickstarter.com/projects/blankmediagames/town-of-salem-the-card-game.

## Prerequirities ##

Node.js

## Running ##

npm install
npm start

## Tests ##

npm test

## Product build ##

npm run build

Master branch ./build/ directory contents is automatically hosted at [http://townofsalemmoderatorhelper.azurewebsites.net/].

## Technonology ##

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app). Uses React & Redux.