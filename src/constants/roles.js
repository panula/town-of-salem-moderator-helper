export const effects = Object.freeze({ HEAL: 'HEAL', ALERT: 'ALERT', MURDER: 'MURDER', CURSE: 'CURSE', EXECUTE: 'EXECUTE'});
export const targets = Object.freeze({ ALL_PLAYERS: 'ALL_PLAYERS', ALL_BUT_MAFIA: 'ALL_BUT_MAFIA', ONLY_PLAYER: 'ONLY_PLAYER', ALL_BUT_SELF: 'ALL_BUT_SELF'});
export const properties = Object.freeze({ NIGHT_IMMUNITY: 'NIGHT_IMMUNITY'});
export const factions = Object.freeze({ TOWN: 'TOWN', MAFIA: 'MAFIA', NEUTRAL: 'NEUTRAL' });
export const roleKeys = Object.freeze({ TOWNIE: 'TOWNIE', DOCTOR: 'DOCTOR', VETERAN: 'VETERAN', MAFIOSO: 'MAFIOSO', SERIAL_KILLER: 'SERIAL_KILLER', WITCH: 'WITCH', JESTER: 'JESTER', GODFATHER: 'GODFATHER', POLITICIAN: 'POLITICIAN', SURVIVOR: 'SURVIVOR', SHERIFF: 'SHERIFF', VIGILANTE: 'VIGILANTE', EXECUTIONER: 'EXECUTIONER'})
export const sheriffSeesAs = Object.freeze({ EVIL: 'EVIL', GOOD: 'GOOD'});

export const roles = Object.freeze({

    // town
    TOWNIE: {
        key: roleKeys.TOWNIE, displayName: 'townie', value: 1, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD, action: {}
    },
    POLITICIAN: {
        key: roleKeys.POLITICIAN, displayName: 'politician', value: -2, faction: factions.TOWN, appearsAs: sheriffSeesAs.EVIL, action: {}
    },
    DOCTOR: {
        key: roleKeys.DOCTOR, displayName: 'doctor', value: 4, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.HEAL, displayName: 'Heal', targetsTo: targets.ALL_PLAYERS, usagesLeft: 2, power: 100 }
    },
    VETERAN: {
        key: roleKeys.VETERAN, displayName: 'veteran', value: 4, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.ALERT, displayName: 'Alert', targetsTo: targets.ONLY_PLAYER, usagesLeft: 2 }
    },
    SURVIVOR: {
        key: roleKeys.SURVIVOR, displayName: 'survivor', value: 4, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD,
        action: {}, property: properties.NIGHT_IMMUNITY
    },
    SHERIFF: {
        key: roleKeys.SHERIFF, displayName: 'sheriff', value: 7, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD, action: {}
    },
    VIGILANTE: {
        key: roleKeys.VIGILANTE, displayName: 'vigilante', value: 5, faction: factions.TOWN, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.MURDER, displayName: 'Shoot', targetsTo: targets.ALL_BUT_SELF, power: 90, usagesLeft: 1 }
    },

    // mafia
    MAFIOSO: {
        key: roleKeys.MAFIOSO, displayName: 'mafioso', value: -6, faction: factions.MAFIA, appearsAs: sheriffSeesAs.EVIL,
        action: { effect: effects.MURDER, displayName: 'Murder', targetsTo: targets.ALL_BUT_MAFIA, power: 90 }
    },
    GODFATHER: {
        key: roleKeys.GODFATHER, displayName: 'godfather', value: -8, faction: factions.MAFIA, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.MURDER, displayName: 'Murder', targetsTo: targets.ALL_BUT_MAFIA, power: 90 }
    },

    // neutrals
    SERIAL_KILLER: {
        key: roleKeys.SERIAL_KILLER, displayName: 'serialkiller', value: -8, faction: factions.NEUTRAL, appearsAs: sheriffSeesAs.EVIL,
        action: { effect: effects.MURDER, displayName: 'Kill', targetsTo: targets.ALL_BUT_SELF, power: 90 },
        property: properties.NIGHT_IMMUNITY
    },
    WITCH: {
        key: roleKeys.WITCH, displayName: 'witch', value: -5, faction: factions.NEUTRAL, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.CURSE, displayName: 'Curse', targetsTo: targets.ALL_BUT_SELF, power: 10 }
    },
    JESTER: {
        key: roleKeys.JESTER, displayName: 'jester', value: -1, faction: factions.NEUTRAL, appearsAs: sheriffSeesAs.GOOD, action: {} 
    },
    EXECUTIONER: {
        key: roleKeys.EXECUTIONER, displayName: 'executioner', value: -4, faction: factions.NEUTRAL, appearsAs: sheriffSeesAs.GOOD,
        action: { effect: effects.EXECUTE, displayName: 'Execute', targetsTo: targets.ALL_BUT_SELF, power: 0}
    }

});

export default roles;