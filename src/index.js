import React from 'react';
import ReactDOM from 'react-dom';
import AppContainer from './components/App/App';
import './index.css';
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'
import app from './store/Reducers'
import undoable from 'redux-undo';
import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap/dist/js/bootstrap.js'


const reducers = combineReducers({
    app: undoable(app)
});

const persistedState = JSON.parse(localStorage.getItem('gameState')) || {};
let store = createStore(
    reducers, persistedState
);

store.subscribe(() => {
    localStorage.setItem('gameState', JSON.stringify(store.getState()));
});

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('root')
)




// TODO PANU: keyboard bindings?
/*
window.onkeydown = (e) => {
    // alert(e.keyCode);
    switch (e.keyCode) {
        case 106: // j
        case 37: // arrow left
        store.dispatch(ActionCreators.undo()) // undo the last action
            break;
        case 107: // k
        case 39: // arrow right
            store.dispatch(ActionCreators.redo()) // redo the last action
            break;
    }
} 
*/
