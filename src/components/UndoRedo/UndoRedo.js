import React from 'react'
import { ActionCreators as UndoActionCreators } from 'redux-undo'
import { connect } from 'react-redux'


let UndoRedo = ({ canUndo, canRedo, past, onUndo, onRedo }) => (
    <div>
      <div>
        <h3>History</h3>
      </div>
      
        <p className='clearfix'>
            <button className='btn btn-default btn-sm' onClick={onUndo} disabled={!canUndo}>
            Undo
            </button>
            
            <button className='btn btn-default btn-sm' onClick={onRedo} disabled={!canRedo}>
            Redo
            </button>
        </p>

        {past.map((p, index) => <div key={index}>{p.lastAction}</div>)}
  </div>
)

const mapStateToProps = (state) => {
  return {
    canUndo: state.app.past.length > 0,
    canRedo: state.app.future.length > 0,
    past: state.app.past.concat(state.app.present)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onUndo: () => dispatch(UndoActionCreators.undo()),
    onRedo: () => dispatch(UndoActionCreators.redo())
  }
}

UndoRedo = connect(
  mapStateToProps,
  mapDispatchToProps
)(UndoRedo)

export default UndoRedo