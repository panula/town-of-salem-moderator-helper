import React from 'react';
import PlayerList from '../PlayerList/PlayerList';
import UndoRedo from '../UndoRedo/UndoRedo'
import { connect } from 'react-redux'
import _ from 'lodash'
import actionTypes from '../../store/actionTypes'

export let App = ({night, started, onNextNight, onStarted, log, onNewGame}) => 
(
  <div className="container-fluid">
    <div className="row">

  <div className="col-sm-10">
    
    <h1>LOL</h1>
    <h1>Town of Salem moderator helper</h1>

    <h2>Players</h2>
    <PlayerList />

    <h2>Status:</h2>
    Night: {night}<br />
    Started: {started ? 'YES' : 'NO'}<br />
    <button className="btn btn-default" onClick={onStarted} disabled={started}>Start</button>
    <button className="btn btn-default" onClick={() => onNextNight()} disabled={!started}>Next night</button>
    <button className="btn btn-default" onClick={() => {localStorage.clear(); window.location.reload();}}>Reset</button>
    <button className="btn btn-default" onClick={onNewGame}>New game</button>
    <h3>Log</h3>
    {log.map((e, i) => 
      <div key={i}>{e}</div>
    )}

    <h2>Checklist</h2>
    <h3>First night</h3>
    <ol>
      <li>Executor (choose target)</li>
      <li>Sheriff (choose player & reveal if player is evil)</li>
      <li>Mafia (open eyes, find out teammates)</li>
    </ol>
    <h3>Next nights</h3>
    <ol>
      <li>Sheriff (choose player & reveal if player is evil)</li>
      <li>Special roles (like doctor, veteran, witch) activate (show the target player number with fingers)</li>
      <li>Consigliere (choose player to find out role)</li>
      <li>Investigator (investigate)</li>
      <li>Mafia (negotiate, murder)</li>
    </ol>

    <h2>Known issues</h2>
    <ul>
      <li>Veteran kills only mafia members on night when veteran is on alert. Should kill all visitors without night immunity.</li>
      <li>Log does not support player name.</li> 
    </ul>

<h2>TODO</h2>
    <ul>
      <li>Implement all roles</li>
      <li>Browser back button as undo</li>
      <li>New feature: Shuffle roles, choose from which roles to shuffle</li>
    </ul>      

    </div>


        
    <div className="col-sm-2">
      <UndoRedo />
      </div>
      </div>
    </div>
    );

const mapDispatchToProps = {
    onNextNight: () => { return { type: actionTypes.NEXT_NIGHT }},
    onStarted: () => { return { type: actionTypes.GAME_START }},
    onNewGame: () => { return { type: actionTypes.NEW_GAME }}
};

const mapStateToProps = (state) => {
  return {
    night: state.app.present.night,
    started: state.app.present.started,
    deckValue: _.sum(state.app.present.players.map(p => p.roleValue)),
    log: state.app.present.log
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(App)








