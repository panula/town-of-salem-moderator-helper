import React from 'react';
import _ from 'lodash'
import {roles, targets, factions} from '../../constants/roles';

let PlayerRow = ({ started, player, onChangeRole, players, onSelectAction, onHang, onToggleAlive, onChangePlayerName }) => {

  const roleButton = (onChangeRole, role, currentRole) => 
  <button type="button" key={role.key} onClick={() => onChangeRole(role)} className={"btn btn-default btn-xs " + (role.key === currentRole.key ? 'active' : '')}>{role.displayName}</button>;

const actionList = (action, players, onSelectAction, player) => {
  if (!player.alive)
    return '';

  let validTargets = [];
  switch (action.targetsTo) {
    case targets.ALL_BUT_SELF:
      validTargets = players.filter(p => p.id !== player.id && p.alive);
      break;
      case targets.ALL_PLAYERS:
      validTargets = players.filter(p => p.alive);
      break;
    case targets.ONLY_PLAYER:
      validTargets = [player];
      break;
    case targets.ALL_BUT_MAFIA:
      validTargets = players.filter(p => p.role.faction !== factions.MAFIA && p.alive);
      break;
    default:
      break;
  }

  if (validTargets.length > 0 ) {
    return <div className="btn-group" role="group" aria-label="...">
      {players.map((t, i) => {
        return <button onClick={() => onSelectAction(t.id, player.id, action.effect, action.power)} type="button" key={i} 
        className={'btn btn-default btn-xs ' + (player.target && player.target.to === t.id ? 'active': '')} disabled={validTargets.filter(v => v.id === t.id).length === 0}>#{t.id}: {t.name}</button>})
      }        
      </div>;
  }
  else if (validTargets.length === 1){
    return <input type="checkbox" />;
  }
};

const hasUsagesLeft = player => !isNaN(player.usagesLeft) && player.alive ? <div>Usages left: {player.usagesLeft}</div> : '';

const getRow = () =>  {
  return  started ?   <tr className={player.alive ? 'success' : 'danger'}>
        <td>{player.id}</td>
        <td><input type='text' onBlur={(e) => onChangePlayerName(player.id, e.target.value)} defaultValue={player.name} /></td>
        <td><input type="checkbox" onChange={() => onToggleAlive(player)} checked={player.alive ? 'checked' : ''} /></td>
        <td>{player.role.displayName}</td>
        <td>{player.alive ? player.role.action.displayName: ''}</td>
        <td>
              {actionList(player.role.action, players, onSelectAction, player)}
              {hasUsagesLeft(player)}
          </td>
          <td>{(player.effects || []).map((e, index) => <div key={index}>{e.effect}</div>)}</td>
          <td>{player.finalEffect ? player.finalEffect.effect : ''}</td>
          <td>{(player.permanentEffects || []).map((e, index) => <div key={index}>{e.effect}</div>)}</td>
          <td>{player.role.appearsAs}</td>
          <td><button className="btn btn-xs btn-default" onClick={() => onHang(player)}>Hang</button></td>
        </tr>
        :
        <tr>
        <td>{player.id}</td>
        <td><input type='text' onBlur={(e) => onChangePlayerName(player.id, e.target.value)} defaultValue={player.name} /></td>
        <td>
          <div className="btn-group" role="group" aria-label="...">
            {_.values(roles).map(role => roleButton(onChangeRole, role, player.role))}
          </div>
        </td>
        <td>{player.role.value}</td>
        <td>{player.role.appearsAs}</td>
        </tr>;
}

  return(
    <tbody>
      {getRow()}
      </tbody>
    )};

export default PlayerRow;



