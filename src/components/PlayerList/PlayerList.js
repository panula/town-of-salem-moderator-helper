import React from 'react'
import { connect } from 'react-redux'
import PlayerRow from '../PlayerRow/PlayerRow';
import _ from 'lodash'
import actionTypes from '../../store/actionTypes';

const getHeaders = (started) => {
  return  started ? <tr>
              <th>#</th>
            <th>Name</th>
            <th>Alive</th>
            <th>Role</th>
            <th>Action</th>
            <th>Target</th>
            <th>Effects</th>
            <th>Final effect</th>
            <th>Permanent effect</th>
            <th>Appears as</th>
            <th></th>
            </tr>
            :
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Role</th>
              <th>Role value</th>
              <th>Appears as</th>
            </tr>;
}


export let PlayerList = ({ started, addPlayer, deckValue, changeRole, players, onSelectAction, onHang, onToggleAlive, onChangePlayerName }) => {
  const button = started ? '' : <button className="btn btn-default btn-xs" onClick={() => addPlayer('Player')}>Add player</button>;
  return (
    <div>
        {button}

        <table className="table">
          <thead>
            {getHeaders(started)}
          </thead>
          {players.map((p, index) => 
            <PlayerRow key={index} 
            player={p}
            players={players} 
            started={started}
            onChangeRole={(a) => changeRole(p.id, a)} 
            onSelectAction={onSelectAction}
            onHang={onHang}
            onToggleAlive={onToggleAlive}
            onChangePlayerName={onChangePlayerName}
            />
          )}

          {started === false ? <tfoot>
            <tr>
            <td colSpan={3}><b>Total deck value:</b></td>
            <td className={Math.abs(deckValue) <= 5 ? 'success' : Math.abs(deckValue) <= 10 ? 'warning' : 'danger' }>{deckValue}</td>
            <td colSpan={4}></td>
            </tr>
            </tfoot> : null}
        </table>
  </div>
)
}

const mapDispatchToProps = {
  addPlayer : (name) => { return { type: actionTypes.ADD_PLAYER, name: name}},
  changeRole : (id, role) => { return { type: actionTypes.CHANGE_ROLE, id: id, role: role}},
  onSelectAction : (to, from, effect, power) => { return { type: actionTypes.SELECT_ACTION, from: from, to: to, effect: effect, power}},
  onHang: (player) => { return { type: actionTypes.HANG_PLAYER, player: player.id }},
  onToggleAlive: player => { return { type: actionTypes.TOGGLE_ALIVE, player: player.id }},
  onChangePlayerName: (player, newName) => { return { type: actionTypes.CHANGE_NAME, player: player.id, name: newName }}
}

export const mapStateToProps = (state) => {
  return {
    players: state.app.present.players.map(p => {
      return {
        id: p.id,
        name: p.name,
        alive: p.alive,
        role: p.role,
        usagesLeft: p.usagesLeft,
        effects: state.app.present.effects.filter(e => e.to === p.id),
        target: state.app.present.effects.filter(e => e.from === p.id)[0],
        finalEffect: _.orderBy(state.app.present.effects.filter(e => e.to === p.id), e => e.power).reverse()[0],
        permanentEffects: state.app.present.permanentEffects.filter(e => e.to === p.id)
      }
    }),
    deckValue: _.sum(state.app.present.players.map(p => p.role.value)),
    started: state.app.present.started
  }
}

let PlayerList2 = connect(
  mapStateToProps,
  mapDispatchToProps
)(PlayerList)



PlayerList.propTypes = {
  addPlayer   : React.PropTypes.func.isRequired,
  changeRole  : React.PropTypes.func.isRequired,
  players     : React.PropTypes.array.isRequired
}


export default PlayerList2