export default (state, action) => {
    const newState =  Object.assign({}, state, {
        lastAction: action.type,
            players: state.players.map((player) => {
            if(player.id === action.id) {
                return Object.assign({}, player, {role: action.role, usagesLeft: action.role.action.usagesLeft ? action.role.action.usagesLeft : undefined});
            }
            return player;
        })
    });
    return newState;
}