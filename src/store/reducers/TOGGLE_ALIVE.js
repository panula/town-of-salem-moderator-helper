export default (state, action) => {
    return Object.assign({}, state, {
        lastAction: action.type,
        players: state.players.map(p => {
            if (action.player === p.id)
                return Object.assign({}, p, { alive: !p.alive});
            return p;
        })
    });
}