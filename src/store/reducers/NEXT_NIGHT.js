import {effects, properties, factions, roleKeys, roles } from '../../constants/roles';

export default (state, action) => {
    
    const onAlert = player => state.effects.filter(e => e.from === player.id && e.effect === effects.ALERT).length > 0;
    const mafiaTriesToKill = player => state.effects.filter(e => e.to === player.id && e.effect === effects.MURDER && state.players.filter(p => p.id === e.from)[0].role.faction === factions.MAFIA).length > 0;
    const isFirstOfFaction = (player, faction) => state.players.filter(p => p.alive && p.role.faction === faction)[0] && state.players.filter(p => p.alive && p.role.faction === faction)[0].id === player.id;
    const veteranKillsThisMafia = player => state.players.filter(p => onAlert(p)).length > 0 && isFirstOfFaction(player, factions.MAFIA) && state.players.filter(p => onAlert(p)).filter(p => mafiaTriesToKill(p)).length > 0; 

    const isImmuneToKill = player => player.role.property === properties.NIGHT_IMMUNITY;
    const isUnderHeal = player => state.effects.filter(e => e.to === player.id && e.effect === effects.HEAL).length > 0;
    const canBeKilled = player => !isImmuneToKill(player) && !isUnderHeal(player) && !onAlert(player); 
    const someoneTriesToKill = player => state.effects.filter(e => e.to === player.id && e.effect === effects.MURDER).length > 0 || veteranKillsThisMafia(player);
    
    const hasUsedAction = player => player.role.action.usagesLeft && state.effects.filter(e => e.from === player.id).length > 0;

    const isAlive = player => !(canBeKilled(player) && someoneTriesToKill(player));

    const playerIsExecutionerAndExecutionerTargetKilled = player => {
        const target = state.permanentEffects.filter(e => e.effect === effects.EXECUTE && e.from === player.id)[0];
        if (!target)
            return null;
        return isAlive(state.players.filter(p => p.id === target.to)[0]) === false;
    }    

    const playerOfRoleIsAlive = (state, role) => state.players.filter(p => p.role.key === role && p.alive).length > 0;
    const playerOfFactionIsAlive = (state, faction) => state.players.filter(p => p.role.faction === faction && p.alive).length > 0;
    const allLivingPlayersAreCursed = state => state.players.filter(p => p.role.key !== roleKeys.WITCH && p.alive && state.permanentEffects.filter(e => e.to === p.id && e.effect === effects.CURSE).length === 0).length === 0;
    const allOthersAreDead = (state, player) => player && state.players.filter(p => p.id !== player.id && p.alive).length === 0;

    const mafiaWins = state =>  playerOfFactionIsAlive(state, factions.MAFIA) &&
                                !playerOfFactionIsAlive(state, factions.NEUTRAL) &&
                                !playerOfFactionIsAlive(state, factions.TOWN);

    const witchWins = state =>  playerOfRoleIsAlive(state, roleKeys.WITCH) && 
                                allLivingPlayersAreCursed(state);
                                
    const serialKillerWins = state =>   playerOfRoleIsAlive(state, roleKeys.SERIAL_KILLER) &&
                                        allOthersAreDead(state, state.players.filter(p => p.role.key === roleKeys.SERIAL_KILLER)[0]);
    
    const townWins = state =>   playerOfFactionIsAlive(state, factions.TOWN) &&
                                !playerOfFactionIsAlive(state, factions.NEUTRAL) &&
                                !playerOfFactionIsAlive(state, factions.MAFIA);

    const roleChangeLogs = [];
    var nextState = 
        Object.assign({}, state, {
        night: state.night + 1,
        lastAction: action.type,
        effects: [],
        permanentEffects: state.permanentEffects.concat(state.effects.filter(e => e.effect === effects.CURSE)),
        players: state.players.map(player => {
            if (!player.alive)
                return Object.assign({}, player);

            let role = player.role;
            if (playerIsExecutionerAndExecutionerTargetKilled(player)) {
                role = roles.JESTER;
                roleChangeLogs.push(player.name + ' turned jester.')
            }

            return Object.assign({}, player, {
                alive: isAlive(player),
                role: Object.assign({}, role),
                usagesLeft: hasUsedAction(player) ? player.usagesLeft - 1 : player.usagesLeft
            });
        }
    )});

    const playersDied = nextState.players.filter(p => state.players.filter(pp => pp.id === p.id)[0].alive !== p.alive);
    const newLogs = playersDied.map(p => "Player " + p.name + " was murdered.").concat(roleChangeLogs);

    if (mafiaWins(nextState))
        newLogs.push('Mafia wins.');
    if (witchWins(nextState))
        newLogs.push('Witch wins.');
    if (serialKillerWins(nextState))
        newLogs.push('Serial killer wins.');
    if (townWins(nextState))
        newLogs.push('Town wins.');


    const eventRow = "Night " + state.night + ": " + (newLogs.length > 0 ?  newLogs.join(' ') : 'Nothing happened.');
    const alllog = state.log.concat([eventRow]); // TODO PANU: ei ole immutable

    nextState.log = alllog;
    return nextState;
}