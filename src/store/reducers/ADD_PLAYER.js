import roles from '../../constants/roles';

export default (state, action) => {
    if (state.players.length === 15)
        return state;
        
    const newId = state.players.length+1;
    const newPlayer = {
        name: action.name + ' ' + newId,
        alive: true,
        id: newId,
        role: roles.TOWNIE
    };
    const finalEffects = Object.assign({}, state.finalEffects);
    finalEffects[newPlayer.id] = [];
    return Object.assign({}, state, {
        lastAction: action.type,
        players: [
            ...state.players,
            newPlayer
        ],
        finalEffects: finalEffects
    });
}