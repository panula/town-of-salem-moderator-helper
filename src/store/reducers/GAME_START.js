export default (state, action) => {
    return Object.assign({}, state, {
        lastAction: action.type,
        started: true
    });
}