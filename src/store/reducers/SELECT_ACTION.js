import {factions, effects} from '../../constants/roles';

export default (state, action) => {

    const affectsTo = action.effect === effects.EXECUTE ? 'permanentEffects' : 'effects';

    const from = (state.players.filter(p => p.id === action.from)[0].role.faction === factions.MAFIA) ?
        state.players.filter(p => p.role.faction === factions.MAFIA).map(p => p.id)
        : 
        [action.from]; 
        
    const oldEffects = state.effects.filter(e => from.filter(from => from === e.from).length === 0);
    const newEffects = from.map(f => {
        return { from: f, to: action.to, effect: action.effect, power: action.power}
    });

    const newState = {};
    newState[affectsTo] = oldEffects.concat(newEffects);
    newState.lastAction = action.type;

    return Object.assign({}, state, newState);
}