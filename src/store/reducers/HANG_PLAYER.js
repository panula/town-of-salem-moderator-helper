import {roleKeys, effects} from '../../constants/roles';

export default (state, action) => {
    const hangedPlayer = state.players.filter(p => p.id === action.player)[0];

    let msg = 'Day ' + state.night + ': ' + hangedPlayer.name + ' was hanged.';
    if (hangedPlayer.role.key === roleKeys.JESTER)
        msg += ' ' + hangedPlayer.name + ' won.';

    const executioner = state.players.filter(p => p.alive && p.role.key === roleKeys.EXECUTIONER)[0];
    if (executioner && state.permanentEffects.filter(e => e.effect === effects.EXECUTE && e.to === hangedPlayer.id).length > 0)
        msg += ' ' + executioner.name + ' won.';

    return Object.assign({}, state, {
        lastAction: action.type,
        players: state.players.map(p => {
            if (action.player === p.id)
                return Object.assign({}, p, { alive: false});
            return p;
        }),
        log: state.log.concat([msg])
    });
}