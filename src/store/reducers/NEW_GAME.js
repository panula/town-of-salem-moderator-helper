import roles from '../../constants/roles';

export default (state, action) => {
    return Object.assign({}, state, {
        lastAction: action.type,
        players: state.players.map(p => {
            return Object.assign({}, p, { role: roles.TOWNIE, alive: true})
        }),
        night: 1,
        started: false,
        effects: [],
        finalEffects: {},
        permanentEffects: [],
        log: []
    });
}