export default (state, action) => {
    return Object.assign({}, state, {
        lastAction: action.type,
        players: state.players.map(p => {
            if (p.id === action.player)
                return Object.assign({}, p, {name: action.name});
            return p;
        })
    });
}