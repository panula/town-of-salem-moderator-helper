import actionTypes from './actionTypes';
import ADD_PLAYER from './reducers/ADD_PLAYER';
import CHANGE_ROLE from './reducers/CHANGE_ROLE';
import GAME_START from './reducers/GAME_START';
import HANG_PLAYER from './reducers/HANG_PLAYER';
import NEXT_NIGHT from './reducers/NEXT_NIGHT';
import SELECT_ACTION from './reducers/SELECT_ACTION';
import TOGGLE_ALIVE from './reducers/TOGGLE_ALIVE';
import CHANGE_NAME from './reducers/CHANGE_NAME';
import NEW_GAME from './reducers/NEW_GAME';

const initialState = {
    players: [],
    night: 1,
    started: false,
    effects: [],
    finalEffects: {},
    permanentEffects: [],
    log: []
}

const app = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_PLAYER:
            return ADD_PLAYER(state, action);
        case actionTypes.NEW_GAME:
            return NEW_GAME(state, action);
        case actionTypes.CHANGE_NAME:
            return CHANGE_NAME(state, action);
        case actionTypes.CHANGE_ROLE:
            return CHANGE_ROLE(state, action);
        case actionTypes.SELECT_ACTION:
            return SELECT_ACTION(state, action);
        case actionTypes.GAME_START:
            return GAME_START(state, action);    
        case actionTypes.HANG_PLAYER:
            return HANG_PLAYER(state, action);
        case actionTypes.NEXT_NIGHT:
            return NEXT_NIGHT(state, action);
        case actionTypes.TOGGLE_ALIVE:
            return TOGGLE_ALIVE(state, action);
        default:
            return state;
    }
}

export default app;