let constants = {
    // game init actions
    ADD_PLAYER:     'ADD_PLAYER',
    CHANGE_ROLE:    'CHANGE_ROLE',
    GAME_START:     'GAME_START',
    CHANGE_NAME:    'CHANGE_NAME',
    NEW_GAME:       'NEW_GAME',


    // gameplay actions
    SELECT_ACTION:  'SELECT_ACTION',
    NEXT_NIGHT:     'NEXT_NIGHT',
    HANG_PLAYER:    'HANG_PLAYER',
    TOGGLE_ALIVE:   'TOGGLE_ALIVE'
    
};

export default constants;